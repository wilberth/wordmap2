<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
header('Content-Type: text/text');
// write full json data array to file unparsed
// each element of the $data array will contain the ppn, 
// $ppn is a string it is read from the first array element

// parse input
$json = file_get_contents('php://input');
$data = json_decode($json, true);

// determine $ppn, prevent injection
if (!is_array($data))
	die("ERROR: input is not array");
if (!count($data))
	die("ERROR: input is array of length < 1:");
if (!array_key_exists("ppn", $data[0]))
	die("ERROR: ppn not in data");
$ppn = $data[0]["ppn"];
if (!ctype_alnum($ppn))
	die("ERROR: ppn non compliant");
if (strlen($ppn)<1 || strlen($ppn)>3)
	die("ERROR: ppn wrong length");


// determine next $iFile
$filenames = glob(sprintf("data/data_%s_??.json", $ppn));
sort($filenames);
if(count($filenames) == 0)
	$iFile = 0;
else {
	$n = preg_match("|data/data_([^_])+_([0-9][0-9]).json|", $filenames[count($filenames)-1], $matches);
	$iFile = ((int)$matches[2])+1; // default is ok too, but let's be explicit
}

// write to file
$filename = sprintf("data/data_%s_%02d.json", $ppn, $iFile);
file_put_contents($filename, $json, FILE_APPEND | LOCK_EX); // unparsed json
print("LOG: success writing to file ".$filename);
?>

