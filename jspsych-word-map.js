/**
* jspsych-word-map
* (C) Wilbert van Ham GPLv3
* version 2 with trial loop
**/

jsPsych.plugins["word-map"] = (function() {
	var plugin = {}
	plugin.info = {
		name: 'word-map',
		description: 'Map of clickable words displayed on given coordinates',
		parameters: {
			words: {
				type: jsPsych.plugins.parameterType.HTML_STRING,
				pretty_name: 'Words',
				default: undefined,
				description: 'The words to be displayed',
			},
			nostart: {
				type: jsPsych.plugins.parameterType.BOOLEAN,
				pretty_name: 'Do not start',
				default: false,
				description: 'Do not start the first phase 1',
			},
			translations: {
				type: jsPsych.plugins.parameterType.HTML_STRING,
				pretty_name: 'Translations',
				default: undefined,
				description: 'The translations to be displayed under the words',
			},
			coordinates: {
				type: jsPsych.plugins.parameterType.COMPLEX,
				pretty_name: 'Coordinates',
				default: undefined,
				description: 'The x-y coordinates of the words',
			},
			yoke_indices:{
				type: jsPsych.plugins.parameterType.INT,
				pretty_name: 'Yoke indices',
				default: undefined,
				description: 'Index list of the yoking. undefined for free trials',
			},
			background_colors: {
				type: jsPsych.plugins.parameterType.COMPLEX,
				pretty_name: 'Background colors',
				default: ["gray"],
				description: 'List of background colors where the click count is the index',
			},
			margin: {
				type: jsPsych.plugins.parameterType.FLOAT,
				pretty_name: 'Margin',
				default: 0,
				description: 'Margin (0-1), 10% means twice 5%, green during selection',
			},
			duration_phase1: {
				type: jsPsych.plugins.parameterType.INT,
				pretty_name: 'Phase 1 durations',
				default: [2000],
				description: 'Times in milliseconds start of trial to green rim.',
			},
			duration_phase2: {
				type: jsPsych.plugins.parameterType.INT,
				pretty_name: 'Phase 2 durations',
				default: [2000],
				description: 'Maximum times in milliseconds from green rim to click.',
			},
			rotation_delay: {
				type: jsPsych.plugins.parameterType.INT,
				pretty_name: 'Rotation delay',
				default: 1000,
					description: 'Time in milliseconds before rotation starts',
			},
			rotation_duration: {
				type: jsPsych.plugins.parameterType.INT,
				pretty_name: 'Rotation duration',
				default: 500,
					description: 'Time in milliseconds from start of rotation to full rotation',
			},
			view_duration: {
				type: jsPsych.plugins.parameterType.INT,
				pretty_name: 'View duration',
				default: 3000,
					description: 'Total time in milliseconds from click to end of viewing translation. = phase 3, includes rotation_delay and rotation_duration',
			},
			post_view_duration: {
				type: jsPsych.plugins.parameterType.INT,
				pretty_name: 'View duration',
				default: 3000,
					description: 'Time in milliseconds after viewing translation = phase 4',
			},
			optout: {
				type: jsPsych.plugins.parameterType.INT,
				pretty_name: 'Opt-out',
				default: 0,
				description: 'Is there a button to abort?',
			},
		},
	}
	
	function startBar(t=2000, color="blue"){
		// time in ms, color in w3 css compatible syntax
		sheet.removeRule(3)
		rule = ` .progress-bar-fill.empty { transition: width ${t}ms linear }`
		sheet.insertRule(rule, 3)
		sheet.removeRule(4)
		rule = ` .progress-bar-fill { background-color: ${color} }`
		sheet.insertRule(rule, 4)
		
		// (re)start transition of bar
		document.getElementsByClassName("progress-bar-fill")[0].classList.remove("empty")
		setTimeout(
			function(){
				document.getElementsByClassName("progress-bar-fill")[0].classList.add("empty")
			}, 40)
	}
	
	var response_list = []
	plugin.trial = function(display_element, trial) {
		// make a style sheet with timing info
		var element = document.createElement('style')
		document.head.appendChild(element)
		sheet = element.sheet
		time = trial.rotation_delay+trial.rotation_duration/2 // delay before switching front to back
		let rule = `.word.rotated .front{ transition: opacity 0s linear ${time}ms !important; }`
		sheet.insertRule(rule, 0)
		rule = `.word.rotated .back{ transition: opacity 0s linear ${time}ms !important; }`
		sheet.insertRule(rule, 1)
		rule = `.word.rotated{ transition: transform ${trial.rotation_duration}ms linear ${trial.rotation_delay}ms; }`
		sheet.insertRule(rule, 2)
		// following two rules are dynamically changed by startBar
		rule = ` .progress-bar-fill.empty { transition: width 1000ms linear }`
		sheet.insertRule(rule, 3)
		rule = ` .progress-bar-fill { background-color: yellow }`
		sheet.insertRule(rule, 4)
		
		
		// html
		display_element.innerHTML = `
		<div id=border></div>
		<div id=cross>⌖</div>
		<div id=wordwrapper></div>
		<div id=barwrapper>
			<div class="progress-bar` + (trial.optout ? " optout" : "") + `">
				<span class="progress-bar-fill" ></span>
			</div>` + 
			(trial.optout ? "<button id=optoutbutton>stop</button>" : "") + `
		</div>`
		
		// optout button is shown
		if(trial.optout)
			document.getElementById("optoutbutton").addEventListener('click', function(e){
				e.currentTarget.classList.add("clicked")
				let response = {
					button_pressed: e.currentTarget.innerHTML,
					rt: performance.now() - start_time,
				}
				response_list.push(response)
				end_trial()
			})
			
		// show words
		wordElements = [] // global list of word elements for later use
		for (let i=0; i<trial.words.length; i++){
			translation = trial.translations[i]
			word = trial.words[i]
		
			x = trial.coordinates[i][0]
			y = trial.coordinates[i][1]

			// create word element
			let div = document.createElement('div')
			div.style = "position: absolute; left: " +(50*trial.margin + 100*(1-trial.margin)*x) + 
				"%; top: " + (50*trial.margin + 100*(1-trial.margin)*y) + "%;"
			div.innerHTML = "<div class=front>"+word+"</div><div class=back>"+translation+"</div>"
			div.classList.add("word")
			// add event listeners to words
			// 1) in mouse grab mode these callbacks are triggered by simulated events from document clicks
			// 2) after phase 2 timeout these callbacks are triggered by simulated events
			// 3) in yoked condition the same happens but with non-random words
			div.addEventListener('click', function() {
				if (typeof trial.yoke_indices!=='undefined'){ // yoked condition
					var iWord = trial.yoke_indices[iTime]
					wordElements[iWord].classList.remove("marked")
					phase3Start(iTime, i, "yoked click") 
				} else
					phase3Start(iTime, i, "free click") 
			})
			let moves = []
			div.addEventListener('mousemove', function(e){
				moves.push([e.clientX, e.clientY])
			})
			document.getElementById("wordwrapper").appendChild(div)
			wordElements.push(div)
		}
				
		// keep track of simulated mouse position
		cross = document.getElementById("cross")
		myX = 0; myY = 0
		function updatePosition(e){
			// update position op pseudo mouse cursor
			if(e!==undefined)
			if(document.pointerLockElement){
				myX += e.movementX
				myY += e.movementY
				//console.log("mouse move with pointerlock")
			} else {
				myX = e.pageX
				myY = e.pageY
				//console.log("mouse move without pointerlock")
			}
			cross.style.left = String(myX)+"px"
			cross.style.top = String(myY)+"px"
			// clientX, layerX, movementX, offsetX, pageX, screenX, x
		}
		document.addEventListener("mousemove", updatePosition)
		
		// simulate click events for click in mouse grab mode
		document.addEventListener("click", function(e){
			let a = document.elementsFromPoint(myX, myY) // list of elements under pseudocursor
			if(a){
				for(e of a){
					if(e.classList.contains("front")){
						e = e.parentNode
						jsPsych.pluginAPI.clearAllTimeouts() // clear end of phase2 beep
					}
					// dispatchevent on .word element as if it was clicked
					const event = new Event('click')
					e.dispatchEvent(event)
				}
			}
		})

		// show timer at bottom of screen and update it every second
		// no longer used, now in css transition .progress-bar-fill.empty
		var start_time = performance.now()
		var iTime = -1 // counter in trial.phase1_duration array, used by phase3Start which is started by click
		function phase1Start(i){
			console.log(`phase 1 start ${i} duration ${trial.duration_phase1[i]}`)
			iTime = i
			//schedule phase 2 starts
			setTimeout(function(){
				phase2Start(i)
			}, trial.duration_phase1[i])
			startBar(trial.duration_phase1[i], "blue")
			//startBar(trial.times[i], "blue")
		}
		
		// start trial after keypress or because phase 1 timeout (only one now)
		function phase2Start(i=-1){
			console.log(`phase 2 start ${i}, max duration ${trial.duration_phase2[i]}`)
			// center mouse
			myX = window.innerWidth/2; myY = window.innerHeight/2
			updatePosition()
			cross.style.visibility = "visible"

			// green border around screen
			document.getElementById("border").style.visibility="visible";
			if (typeof trial.yoke_indices!=='undefined'){ // yoked condition
				let iWord = trial.yoke_indices[i]
				wordElements[iWord].classList.add("marked")
			}

			// unpleasant sound iof no word was clicked, will be canceled by click
			let t = jsPsych.pluginAPI.setTimeout(function(){
				var wordIndexType = "free"
				if (typeof trial.yoke_indices!=='undefined'){ // yoked condition
					var iWord = trial.yoke_indices[i]
					eventType = "yoked timeout"
					wordElements[iWord].classList.remove("marked")
				} else {
					unpleasantSound()
					// free condition random word
					var iWord = Math.floor(Math.random() * wordElements.length)
					eventType = "free timeout"
				}
				phase3Start(i, iWord, eventType)
			}, trial.duration_phase2[i])
			startBar(trial.phase2_duration, "green")
		}
		
		function phase3Start(iTime, iWord, eventType){
			// iTime is the consecutive counter in the duration list
			// iWord is the index in the word list
			// function is called by:
			// - click
			// - timeout
			// - yoke
			console.log(`phase 3 start on word ${iWord} due to ${eventType}`)
			let div = wordElements[iWord]
			//schedule phase 1 start if this is not the last word
			if (iTime < trial.duration_phase1.length-1){
				setTimeout(function(){
					phase1Start(iTime+1)
					// end phase 4
					wordElements[iWord].style.fontWeight = "normal"
				}, trial.view_duration + trial.post_view_duration)
			} else
				// schedule end of plugin 3 seconds after the last trial start
				// if this time is too short, you will miss the last data point
				setTimeout(function(){
					console.log("end")
					end_trial()
				}, trial.view_duration + trial.post_view_duration + 1000) 

			// prevent clicking with invisible pseudo cursor
			if(cross.style.visibility != "visible")
				return
			// make cursor invisible after clicking
			cross.style.visibility = "hidden"
			
			// make backgroundcolor more blue
			let index = 0
			if (div.bgColorIndex !== undefined)
				index = div.bgColorIndex
			div.bgColorIndex = index + 1
			div.childNodes[0].style.backgroundColor = trial.background_colors[index+1]
			
			// start rotation
			div.classList.add("clicked") // remains true permanently after clicking once
			div.classList.add("rotated") // turned off after a while
			setTimeout(function(){
				div.classList.remove("rotated")
			}, trial.view_duration)
			
			// remove green border
			document.getElementById("border").style.visibility = "hidden";
			
			// halt progressbar
			document.getElementsByClassName("progress-bar-fill")[0].classList.remove("empty")
			
			// save data
			let response = {
				word_front: div.firstChild.textContent,
				word_back: div.lastChild.textContent,
				word_index: iWord,
				event_type: eventType,
				rt: performance.now() - start_time,
				//mouse_path: moves,
			}
			response_list.push(response)
		}
		
		// start phase 1:
		if(!trial.nostart)
			phase1Start(0)
		

		// function to end trial when it is time or button is pressed
		function end_trial() {
			// clean up
			jsPsych.pluginAPI.cancelAllKeyboardResponses()
			jsPsych.pluginAPI.clearAllTimeouts() // not for repeating intervals
			display_element.innerHTML = ""
			data = {response_list: response_list}
			jsPsych.finishTrial(data)
		}
	}
	return plugin
})()

// stand alone helper functions
context = new AudioContext()
function sound(frequency=440, duration=0.200, volume=1, type="sine", delay=0) {
	const oscillator = context.createOscillator()
	const gain = context.createGain()
	oscillator.connect(gain)
	oscillator.frequency.value = frequency
	oscillator.type = "square" // square for unpleasant sound, sine for pleasant sound
	gain.connect(context.destination)
	gain.gain.value = volume
	oscillator.start(context.currentTime + delay)
	oscillator.stop(context.currentTime + delay + duration)
}
function unpleasantSound(delay=0){
	sound(380, 0.3, 0.5, "square", delay)
}
function pleasantSound(delay=0){
	sound(880, 0.3, 0.2, "sine", delay)
}
