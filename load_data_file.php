<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
header('Content-Type: application/json'); // make sure to include quotation marks in log messages
// load last data file for certain ppn
// ppn is given url encoded (get/post)

// get ppn
$ppn = "0";
if (!array_key_exists("ppn", $_REQUEST))
	die('"ERROR: ppn not in request"');
$ppn = $_REQUEST['ppn'];
if (!ctype_alnum($ppn))
	die('"ERROR: ppn non compliant"');
if (strlen($ppn)<1 || strlen($ppn)>3)
	die('"ERROR: ppn wrong length"');

// determine last $iFile
$filenames = glob(sprintf("data/data_%s_??.json", $ppn));
sort($filenames);
if(count($filenames) == 0)
	die('"ERROR: no data available"');
else {
	$n = preg_match("|data/data_([^_])+_([0-9][0-9]).json|", $filenames[count($filenames)-1], $matches);
	$iFile = ((int)$matches[2]); // default is ok too, but let's be explicit
}

$filename = sprintf("data/data_%s_%02d.json", $ppn, $iFile);
readfile($filename, LOCK_EX); // unparsed json

?>
