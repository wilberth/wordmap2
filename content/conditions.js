conditions = {
	test: { // index, this is the ppn of this trial
		sortMatch: "S02", // not used, ppn of sort match
		yoker: null, // which ppn yokes this ppn
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  B},
		]
	},
	S01: { // index, this is the ppn of this trial
		sortMatch: "S02", // not used, ppn of sort match
		yoker: null, // which ppn yokes this ppn
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  B},
			{ iTrial: 2, follow: false, permute: true, words:  D},
		]
	},
	S02: { 
		sortMatch: "S01",
		yoker: null, 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  B},
			{ iTrial: 2, follow: false, permute: false, words:  D},
		]
	},
	S03: { // index, this is the ppn of this trial
		sortMatch: "S04", // not used, ppn of sort match
		yoker: null, // which ppn yokes this ppn
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  C},
			{ iTrial: 2, follow: false, permute: true, words:  A},
		]
	},
	S04: { 
		sortMatch: "S03",
		yoker: null, 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  C},
			{ iTrial: 2, follow: false, permute: false, words:  A},
		]
	},
	P01: {
		sortMatch: "P03",
		yoker: "S01", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  C},
			{ iTrial: 1, follow: true,  permute: false, words:  B},
			{ iTrial: 2, follow: false, permute: true, words:  A},
			{ iTrial: 3, follow: true , permute: true, words:  D},
		]
	},
	P02: {
		sortMatch: "P04",
		yoker: "P01", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  C},
			{ iTrial: 1, follow: false,  permute: false, words:  D},
			{ iTrial: 2, follow: true, permute: true, words:  A},
			{ iTrial: 3, follow: false, permute: true, words:  B},
		]
	},
	P03: {
		sortMatch: "P01",
		yoker: "S02", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  C},
			{ iTrial: 1, follow: true,  permute: true, words:  B},
			{ iTrial: 2, follow: false, permute: false, words:  A},
			{ iTrial: 3, follow: true , permute: false, words:  D},
		]
	},
	P04: {
		sortMatch: "P02",
		yoker: "P03", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  C},
			{ iTrial: 1, follow: false,  permute: true, words:  D},
			{ iTrial: 2, follow: true, permute: false, words:  A},
			{ iTrial: 3, follow: false, permute: false, words:  B},
		]
	},
	P05: {
		sortMatch: "P07",
		yoker: "P02", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  D},
			{ iTrial: 1, follow: false,  permute: true, words:  A},
			{ iTrial: 2, follow: true, permute: true, words:  B},
			{ iTrial: 3, follow: false, permute: false, words:  C},
		]
	},
	P06: {
		sortMatch: "P08",
		yoker: "P05", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  B},
			{ iTrial: 1, follow: true,  permute: true, words:  A},
			{ iTrial: 2, follow: false, permute: true, words:  D},
			{ iTrial: 3, follow: true, permute: false, words:  C},
		]
	},
	P07: {
		sortMatch: "P05",
		yoker: "P04", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  D},
			{ iTrial: 1, follow: false,  permute: false, words:  A},
			{ iTrial: 2, follow: true, permute: false, words:  B},
			{ iTrial: 3, follow: false, permute: true, words:  C},
		]
	},
	P08: {
		sortMatch: "P06",
		yoker: "P07", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  B},
			{ iTrial: 1, follow: true,  permute: false, words:  A},
			{ iTrial: 2, follow: false, permute: false, words:  D},
			{ iTrial: 3, follow: true, permute: true, words:  C},
		]
	},
	P09: {
		sortMatch: "P11",
		yoker: "P06", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  B},
			{ iTrial: 1, follow: false,  permute: true, words:  C},
			{ iTrial: 2, follow: false, permute: false, words:  A},
			{ iTrial: 3, follow: true, permute: true, words:  D},
		]
	},
	P10: {
		sortMatch: "P12",
		yoker: "P09", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  D},
			{ iTrial: 1, follow: true,  permute: true, words:  C},
			{ iTrial: 2, follow: true, permute: false, words:  A},
			{ iTrial: 3, follow: false, permute: true, words:  B},
		]
	},
	P11: {
		sortMatch: "P09",
		yoker: "P08", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  B},
			{ iTrial: 1, follow: false,  permute: false, words:  C},
			{ iTrial: 2, follow: false, permute: true, words:  A},
			{ iTrial: 3, follow: true, permute: false, words:  D},
		]
	},
	P12: {
		sortMatch: "P10",
		yoker: "P11", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  D},
			{ iTrial: 1, follow: true,  permute: false, words:  C},
			{ iTrial: 2, follow: true, permute: true, words:  A},
			{ iTrial: 3, follow: false, permute: false, words:  B},
		]
	},
	P13: {
		sortMatch: "P15",
		yoker: "P10", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  A},
			{ iTrial: 1, follow: true,  permute: false, words:  D},
			{ iTrial: 2, follow: true, permute: true, words:  B},
			{ iTrial: 3, follow: false, permute: true, words:  C},
		]
	},
	P14: {
		sortMatch: "P16",
		yoker: "P13", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  A},
			{ iTrial: 1, follow: false,  permute: false, words:  B},
			{ iTrial: 2, follow: false, permute: true, words:  D},
			{ iTrial: 3, follow: true, permute: true, words:  C},
		]
	},
	P15: {
		sortMatch: "P13",
		yoker: "P12", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  A},
			{ iTrial: 1, follow: true,  permute: true, words:  D},
			{ iTrial: 2, follow: true, permute: false, words:  B},
			{ iTrial: 3, follow: false, permute: false, words:  C},
		]
	},
	P16: {
		sortMatch: "P14",
		yoker: "P15", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  A},
			{ iTrial: 1, follow: false,  permute: true, words:  B},
			{ iTrial: 2, follow: false, permute: false, words:  D},
			{ iTrial: 3, follow: true, permute: false, words:  C},
		]
	},
	P17: {
		sortMatch: "P19",
		yoker: "P14", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  C},
			{ iTrial: 1, follow: false,  permute: true, words:  A},
			{ iTrial: 2, follow: true, permute: true, words:  D},
			{ iTrial: 3, follow: true, permute: false, words:  B},
		]
	},
	P18: {
		sortMatch: "P20",
		yoker: "P17", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  C},
			{ iTrial: 1, follow: true,  permute: true, words:  A},
			{ iTrial: 2, follow: false, permute: true, words:  B},
			{ iTrial: 3, follow: false, permute: false, words:  D},
		]
	},
	P19: {
		sortMatch: "P17",
		yoker: "P16", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  C},
			{ iTrial: 1, follow: false,  permute: false, words:  A},
			{ iTrial: 2, follow: true, permute: false, words:  D},
			{ iTrial: 3, follow: true, permute: true, words:  B},
		]
	},
	P20: {
		sortMatch: "P18",
		yoker: "P19", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  C},
			{ iTrial: 1, follow: true,  permute: false, words:  A},
			{ iTrial: 2, follow: false, permute: false, words:  B},
			{ iTrial: 3, follow: false, permute: true, words:  D},
		]
	},
	P21: {
		sortMatch: "P23",
		yoker: "P18", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  D},
			{ iTrial: 1, follow: true,  permute: true, words:  B},
			{ iTrial: 2, follow: false, permute: false, words:  A},
			{ iTrial: 3, follow: false, permute: true, words:  C},
		]
	},
	P22: {
		sortMatch: "P24",
		yoker: "P21", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  B},
			{ iTrial: 1, follow: false,  permute: true, words:  D},
			{ iTrial: 2, follow: true, permute: false, words:  A},
			{ iTrial: 3, follow: true, permute: true, words:  C},
		]
	},
	P23: {
		sortMatch: "P21",
		yoker: "P20", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  D},
			{ iTrial: 1, follow: true,  permute: false, words:  B},
			{ iTrial: 2, follow: false, permute: true, words:  A},
			{ iTrial: 3, follow: false, permute: false, words:  C},
		]
	},
	P24: {
		sortMatch: "P22",
		yoker: "P23", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  B},
			{ iTrial: 1, follow: false,  permute: false, words:  D},
			{ iTrial: 2, follow: true, permute: true, words:  A},
			{ iTrial: 3, follow: true, permute: false, words:  C},
		]
  },
	P25: {
		sortMatch: "P27",
		yoker: "S03", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  D},
			{ iTrial: 1, follow: true,  permute: false, words:  C},
			{ iTrial: 2, follow: false, permute: true, words:  B},
			{ iTrial: 3, follow: true , permute: true, words:  A},
		]
	},
	P26: {
		sortMatch: "P28",
		yoker: "P25", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  D},
			{ iTrial: 1, follow: false,  permute: false, words:  A},
			{ iTrial: 2, follow: true, permute: true, words:  B},
			{ iTrial: 3, follow: false, permute: true, words:  C},
		]
	},
	P27: {
		sortMatch: "P25",
		yoker: "S04", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  D},
			{ iTrial: 1, follow: true,  permute: true, words:  C},
			{ iTrial: 2, follow: false, permute: false, words:  B},
			{ iTrial: 3, follow: true , permute: false, words:  A},
		]
	},
	P28: {
		sortMatch: "P26",
		yoker: "P27", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  D},
			{ iTrial: 1, follow: false,  permute: true, words:  A},
			{ iTrial: 2, follow: true, permute: false, words:  B},
			{ iTrial: 3, follow: false, permute: false, words:  C},
		]
	},
	P29: {
		sortMatch: "P31",
		yoker: "P26", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  A},
			{ iTrial: 1, follow: false,  permute: true, words:  B},
			{ iTrial: 2, follow: true, permute: true, words:  C},
			{ iTrial: 3, follow: false, permute: false, words:  D},
		]
	},
	P30: {
		sortMatch: "P32",
		yoker: "P29", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  C},
			{ iTrial: 1, follow: true,  permute: true, words:  B},
			{ iTrial: 2, follow: false, permute: true, words:  A},
			{ iTrial: 3, follow: true, permute: false, words:  D},
		]
	},
	P31: {
		sortMatch: "P29",
		yoker: "P28", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  A},
			{ iTrial: 1, follow: false,  permute: false, words:  B},
			{ iTrial: 2, follow: true, permute: false, words:  C},
			{ iTrial: 3, follow: false, permute: true, words:  D},
		]
	},
	P32: {
		sortMatch: "P30",
		yoker: "P31", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  C},
			{ iTrial: 1, follow: true,  permute: false, words:  B},
			{ iTrial: 2, follow: false, permute: false, words:  A},
			{ iTrial: 3, follow: true, permute: true, words:  D},
		]
	},
	P33: {
		sortMatch: "P35",
		yoker: "P30", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  C},
			{ iTrial: 1, follow: false,  permute: true, words:  D},
			{ iTrial: 2, follow: false, permute: false, words:  B},
			{ iTrial: 3, follow: true, permute: true, words:  A},
		]
	},
	P34: {
		sortMatch: "P36",
		yoker: "P33", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  A},
			{ iTrial: 1, follow: true,  permute: true, words:  D},
			{ iTrial: 2, follow: true, permute: false, words:  B},
			{ iTrial: 3, follow: false, permute: true, words:  C},
		]
	},
	P35: {
		sortMatch: "P33",
		yoker: "P32", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  C},
			{ iTrial: 1, follow: false,  permute: false, words:  D},
			{ iTrial: 2, follow: false, permute: true, words:  B},
			{ iTrial: 3, follow: true, permute: false, words:  A},
		]
	},
	P36: {
		sortMatch: "P34",
		yoker: "P35", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  A},
			{ iTrial: 1, follow: true,  permute: false, words:  D},
			{ iTrial: 2, follow: true, permute: true, words:  B},
			{ iTrial: 3, follow: false, permute: false, words:  C},
		]
	},
	P37: {
		sortMatch: "P39",
		yoker: "P34", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  B},
			{ iTrial: 1, follow: true,  permute: false, words:  A},
			{ iTrial: 2, follow: true, permute: true, words:  C},
			{ iTrial: 3, follow: false, permute: true, words:  D},
		]
	},
	P38: {
		sortMatch: "P40",
		yoker: "P37", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  B},
			{ iTrial: 1, follow: false,  permute: false, words:  C},
			{ iTrial: 2, follow: false, permute: true, words:  A},
			{ iTrial: 3, follow: true, permute: true, words:  D},
		]
	},
	P39: {
		sortMatch: "P37",
		yoker: "P36", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  B},
			{ iTrial: 1, follow: true,  permute: true, words:  A},
			{ iTrial: 2, follow: true, permute: false, words:  C},
			{ iTrial: 3, follow: false, permute: false, words:  D},
		]
	},
	P40: {
		sortMatch: "P38",
		yoker: "P39", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  B},
			{ iTrial: 1, follow: false,  permute: true, words:  C},
			{ iTrial: 2, follow: false, permute: false, words:  A},
			{ iTrial: 3, follow: true, permute: false, words:  D},
		]
	},
	P41: {
		sortMatch: "P43",
		yoker: "P38", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  D},
			{ iTrial: 1, follow: false,  permute: true, words:  B},
			{ iTrial: 2, follow: true, permute: true, words:  A},
			{ iTrial: 3, follow: true, permute: false, words:  C},
		]
	},
	P42: {
		sortMatch: "P44",
		yoker: "P41", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  D},
			{ iTrial: 1, follow: true,  permute: true, words:  B},
			{ iTrial: 2, follow: false, permute: true, words:  C},
			{ iTrial: 3, follow: false, permute: false, words:  A},
		]
	},
	P43: {
		sortMatch: "P41",
		yoker: "P40", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  D},
			{ iTrial: 1, follow: false,  permute: false, words:  B},
			{ iTrial: 2, follow: true, permute: false, words:  A},
			{ iTrial: 3, follow: true, permute: true, words:  C},
		]
	},
	P44: {
		sortMatch: "P42",
		yoker: "P43", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  D},
			{ iTrial: 1, follow: true,  permute: false, words:  B},
			{ iTrial: 2, follow: false, permute: false, words:  C},
			{ iTrial: 3, follow: false, permute: true, words:  A},
		]
	},
	P45: {
		sortMatch: "P47",
		yoker: "P42", 
		trials:[
			{ iTrial: 0, follow: true, permute: false, words:  A},
			{ iTrial: 1, follow: true,  permute: true, words:  C},
			{ iTrial: 2, follow: false, permute: false, words:  B},
			{ iTrial: 3, follow: false, permute: true, words:  D},
		]
	},
	P46: {
		sortMatch: "P48",
		yoker: "P45", 
		trials:[
			{ iTrial: 0, follow: false, permute: false, words:  C},
			{ iTrial: 1, follow: false,  permute: true, words:  A},
			{ iTrial: 2, follow: true, permute: false, words:  B},
			{ iTrial: 3, follow: true, permute: true, words:  D},
		]
	},
	P47: {
		sortMatch: "P45",
		yoker: "P44", 
		trials:[
			{ iTrial: 0, follow: true, permute: true, words:  A},
			{ iTrial: 1, follow: true,  permute: false, words:  C},
			{ iTrial: 2, follow: false, permute: true, words:  B},
			{ iTrial: 3, follow: false, permute: false, words:  D},
		]
	},
	P48: {
		sortMatch: "P46",
		yoker: "P47", 
		trials:[
			{ iTrial: 0, follow: false, permute: true, words:  C},
			{ iTrial: 1, follow: false,  permute: false, words:  A},
			{ iTrial: 2, follow: true, permute: true, words:  B},
			{ iTrial: 3, follow: true, permute: false, words:  D},
		]
  }
}
