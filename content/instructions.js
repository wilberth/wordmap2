instructions_text = {
	test: `
<h1>We zijn benieuwd hoe veel woorden je net geleerd hebt.</h1>
<p>Je ziet zo alle woorden één voor één op het scherm samen met een tekstveld. Probeer de juiste vertaling te typen.</p>
<p>Belangrijk: Waag altijd een gok, ook als je het niet 100% zeker weet. Kun je maar een deel van het woord herinneren? Vul het alsnog in. </p>
<p></p>
<p>Druk op een toets om te beginnen.</p>
`,
	test2: `
<h1>We hebben nog een tweede test.</h1>
<p>Je ziet zo telkens één woordpaar op het scherm. Soms zijn de woorden de juiste vertaling van elkaar, soms niet.</p>
<p>Jij moet voor elk woordpaar aangeven of het de juiste of een verkeerde vertaling is.</p> 
<p>Je hebt 5 seconden de tijd. Als je geen antwoord geeft, gaat het experiment vanzelf door naar het volgende woordpaar.</p>
<p></p>
<p>Druk op een toets om te beginnen.</p>
`,
	free: `
<h1>Instructie</h1>
<p>In het volgende blok zie je 25 woorden op het scherm.</p>
<p>Je hebt een paar seconden om de woorden te bekijken en een keuze te maken over welk woord je als volgende wilt oefenen.</p>
<p>Hoe lang je hebt zie je aan de aflopende blauwe balk onderaan het scherm.</p>
<p>Als de balk leeg is komt er een roos in het midden van het scherm. Gebruik deze om op het woord te klikken dat je hebt gekozen.</p>
<p>Het woord draait nu om en je ziet de vertaling. Onthoud deze goed.</p>
<p></p>
<p>Je hebt tien minuten om zoveel mogelijk woorden te leren. Druk op een toets om te beginnen.</p>
`,
	follow: `
<h1>Instructie</h1>
<p>In het volgende blok zie je 25 woorden op het scherm.</p>
<p>Je hebt een paar seconden om de woorden te bekijken. Erna wordt één woord groen gemarkeerd. Die ga je als volgende oefenen.</p>
<p>Hoe lang je hebt zie je aan de aflopende blauwe balk onderaan het scherm.</p>
<p>Als de balk leeg is, komt er een roos in het midden van het scherm. Gebruik deze om op het gemarkeerde woord te klikken.</p>
<p>Het woord draait nu om en je ziet de vertaling. Onthoud deze goed.</p>
<p></p>
<p>Je hebt tien minuten om zoveel mogelijk woorden te leren. Druk op een toets om te beginnen.</p>
`
}
